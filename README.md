# README #

Программа SybaseLogRecovery предназначена для восстановления синтаксиса и форматирования SQL запроса.

### Версия и страница репозитория ###

* Sybase Log Recovery (Liberty Insurance Russia)
* 1.3.1
* [Официальная страница](https://bitbucket.org/trinitron/sybaselogrecovery/overview)
* автор: IVKalyakin@yandex.ru

### Пример использования ###

Входной формат данных может представлять вид:
~~~
Execute sql [b2b_travel_ins_upd_policy @account_id=43391, @account_client_id=43391, @date_out='2017-11-02 17:02:10', @policy_id=null, @contract_id=4500000954, @currency_type_id=0, @start_date='2017-12-01 00:00:00', @end_date='2017-12-31 00:00:00', @product_id=14065, @document_type_id=2, @policy_number='', @cover_days_count=31, @insurance_premium_total=2230.0, @create_date='2017-11-02 00:00:00', @document_kind_id=47, @insured_name_rus='', @result_status=null, @result_value=null, @result_message=null, @id_object=null, @id_master=null, @id_calculation=null, @currency_policy_id=0, @order_date=null, @comments='', @special_conditions='', @luggage_quantity=0]
~~~

Результирующий (восстановленый) запрос:
~~~
declare @policy_number varchar(30)
declare @create_date datetime
declare @result_status int
declare @result_value numeric(12,0)
declare @result_message varchar(255)

execute b2b_travel_ins_upd_policy
@account_id=43391,
@account_client_id=43391,
@date_out="2017-11-02 17:02:10",
@policy_id=null,
@contract_id=4500000954,
@currency_type_id=0,
@start_date="2017-12-01 00:00:00",
@end_date="2017-12-31 00:00:00",
@product_id=14065,
@document_type_id=2,
@policy_number=@policy_number out,
@cover_days_count=31,
@insurance_premium_total=2230.0,
@create_date=@create_date out,
@document_kind_id=47,
@insured_name_rus=NULL,
@result_status=@result_status out,
@result_value=@result_value out,
@result_message=@result_message out,
@id_object=null,
@id_master=null,
@id_calculation=null,
@currency_policy_id=0,
@order_date=null,
@comments=NULL,
@special_conditions=NULL,
@luggage_quantity=0,
@ignore_consent=NULL
~~~

Программа использует подключение к базе данных Sybase ASE 15 и выше.


### На какие кнопки нажимать чтобы заработало? ###

* Open the application
* Add Java query debugger to the left column (входной лог)
* Press restore bunnon (Восстановить)
* Get and copy validated executable sql string from the right column (восстановленный sql)
* Success!