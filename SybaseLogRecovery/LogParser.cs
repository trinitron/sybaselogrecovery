﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace SybaseLogRecovery
{
    public class LogParser
    {
        private readonly string regProcName = @"(?<=Execute SQL\s+\[?\s?)[\w]{1,32}";
                                                                                                            //дата текстовая                //простой текст             //дата формат sql                   //номер полиса
        private readonly string regParams = @"(?<proc_name>@\w+)\s?=\s?\'?(?<proc_value>(\w{3}\s\w{3}\s\d{2}\s\d{2}:\d{2}:\d{2}\s\w{3}\s\d{4})|([\w\d-\:\s\.]){1,100})|(\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2})|(\d{3}-\d{2}-\d{6}-\d{2})";

        //private List<string> matchElements = {"(\w{3}\s\w{3}\s\d{2}\s\d{2}:\d{2}:\d{2}\s\w{3}\s\d{4})", ""};

        private readonly string[] dateFormats =
        {
            "ddd MMM dd HH:mm:ss 'MSK' yyyy",   //Sun Feb 05 23:59:59 MSK 2017
            "yyyy-mm-dd HH:mm:ss"               //2017-07-27 00:00:00
        };
        public dataDataSet.DataTable1DataTable DT { get; set; }
        public Dictionary<string, string> ParsedParams { get; set; }
        public Dictionary<string, string> AbsentParams { get; set; }
        public Dictionary<string, string> ErrorParams { get; set; }
        public string DbProcName { get; set; }
        public int DbParamCount { get; set; }
        public string ResultSql { get; set; }

        public string ReplaceValue { get; set; }

        public bool SkipAt { get; set; }
        public bool OutDeclare { get; set; }

        public LogParser(bool at, bool ou)
        {
            SkipAt = at;
            OutDeclare = ou;
        }


        public void StartParse(string str, string replaceValue)
        {
            ParsedParams = new Dictionary<string, string>();
            AbsentParams = new Dictionary<string, string>();
            ErrorParams = new Dictionary<string, string>();

            ReplaceValue = replaceValue;
            Match match = Regex.Match(str, regProcName, RegexOptions.IgnoreCase);
            Match match2 = Regex.Match(str, @"[\w]{1,32}", RegexOptions.IgnoreCase);

            if (match.Success || match2.Success)
            {
                var procName = match.Success ? match.Value : match2.Value;
                DbProcName = procName;
                ProcedureRequest(procName);

                if (DT.Rows.Count > 0)
                {
                    //TestFill(DT);
                    ParseParams(str);
                    CombineSql();
                    FillInformation();
                }
                else
                {
                    ResultSql = "ошибка восстановления процедуры " + procName + "";
                }


            }
            else
            {
                MessageBox.Show(@"Проверьте наличие ""Execute SQL <имя_процедуры>"" в тексте исходного лога!", "Ошибка распознавания",
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        public void ParseParams(string str)
        {
            var rgprm = SkipAt ? regParams.Replace("@", "") : regParams;
            Match match = Regex.Match(str, rgprm, RegexOptions.IgnoreCase);
            while (match.Success)
            {
                Group pn = match.Groups["proc_name"];
                Group pv = match.Groups["proc_value"];

                var pnValue = SkipAt ? ("@" + pn.Value) : pn.Value;
                if (ParsedParams.ContainsKey(pnValue))
                {
                    if (!ErrorParams.ContainsKey(pnValue))
                    {
                        ErrorParams.Add(pnValue, "дубликат параметра");
                    }
                    match = match.NextMatch();
                }
                else
                {
                    ParsedParams.Add(pnValue, pv.Value);
                }
                match = match.NextMatch();
            }


        }

        public void CombineSql(int mode = 0)
        {
            ResultSql = String.Empty;
            /*Декларирование OUT параметров*/
            if (OutDeclare)
            {
                foreach (var outParam in DT.Where(x => x.paramOut == 2))
                {
                    ResultSql += "declare " + outParam.paramName + " " + outParam.paramType;
                    if (!outParam.IsoutPrecNull() && !outParam.IsoutScaleNull())
                        ResultSql += "(" + outParam.outPrec + "," + outParam.outScale + ")";
                    if (!outParam.IsoutLengthNull() && outParam.userType == 2)
                        ResultSql += "(" + outParam.outLength + ")";
                    ResultSql += "\r\n";
                }
                if (DT.Count(x => x.paramOut == 2) > 0)
                    ResultSql += "\r\n";
            }

            /*Обработка IN параметров*/
            ResultSql += "execute " + DbProcName + "\r\n";
            for (int i = 0; i < DT.Rows.Count; i++)
            {
                string paramString = String.Empty;
                ResultSql += DT.Rows[i]["ParamName"] + "=";

                // Если ParsedParams не имеет параметра который есть в БД или это OUT параметр, то:
                if (!ParsedParams.ContainsKey(DT.Rows[i]["ParamName"].ToString()) || DT.Rows[i]["paramOut"].ToString() == "2")
                {
                    if (OutDeclare && DT.Rows[i]["paramOut"].ToString() == "2")
                    {
                        ResultSql += DT.Rows[i]["ParamName"] + " out";
                    }
                    else
                    {
                        ResultSql += ReplaceValue;
                        AbsentParams.Add(DT.Rows[i]["ParamName"].ToString(), "не найден");
                    }

                }
                else
                {
                    string paramValue = ParsedParams[DT.Rows[i]["ParamName"].ToString()];
                    switch (DT.Rows[i]["ParamType"].ToString())
                    {
                        case "varchar":
                        case "char":
                        case "text":
                        case "nchar":
                        case "nvarchar":
                            paramString = @"""" + paramValue + @"""";
                            if (String.IsNullOrEmpty(paramValue.Trim()))
                            {
                                ErrorParams.Add(DT.Rows[i]["ParamName"].ToString(), "пустая строка");
                            }
                            break;
                        case "int":
                        case "numeric":
                        case "tinyint":
                        case "smallint":
                        case "float":
                        case "real":
                        case "decimal":
                            paramString = paramValue;
                            double pr;
                            if (!double.TryParse(paramValue, NumberStyles.Number, CultureInfo.InvariantCulture, out pr) && !paramValue.Contains("null"))
                            {
                                if (!ErrorParams.ContainsKey(DT.Rows[i]["ParamName"].ToString()))
                                {
                                    ErrorParams.Add(DT.Rows[i]["ParamName"].ToString(), "тип не double");
                                }
                                //paramString += " /*wrong_type*/";
                            }
                            break;
                        case "datetime":
                        case "date":
                        case "time":
                            paramString = @"""" + DateParse(paramValue).ToString("yyyy-MM-dd HH:mm:ss") + @"""";
                            if (DateParse(paramValue)==DateTime.Now)
                            {
                                ErrorParams.Add(DT.Rows[i]["ParamName"].ToString(), "тип не datetime");
                            }
                            break;
                        case "bit":
                            paramString = paramValue.Replace("true", "1").Replace("false", "0");
                            break;
                        default:
                            paramString = @"""" + paramValue + @""" /*unrecognized_type*/";
                            if (!ErrorParams.ContainsKey(paramValue))
                            {
                                ErrorParams.Add(paramValue, "нераспознанный тип");
                            }
                            break;
                    }
                    if (paramValue.Contains("null"))
                    {
                        paramString = paramValue;
                    }
                    if (paramValue.Contains("true") || paramValue.Contains("false"))
                    {
                        paramString = paramValue.Replace("true", "1").Replace("false", "0");
                    }

                }
                ResultSql += paramString;
                if (i < DT.Rows.Count - 1)
                {
                    ResultSql += ",\r\n";
                }




            }
        }

        public void ProcedureRequest(string procedureName)
        {
            DT = new dataDataSet.DataTable1DataTable();
            var ta = new dataDataSetTableAdapters.DataTable1TableAdapter();
            try
            {
                ta.Fill(DT, procedureName);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка работы с базой данных", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }



        /// <summary>
        /// https://msdn.microsoft.com/en-us/library/8kb3ddd4(v=vs.110).aspx
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public DateTime DateParse(string str)
        {
            DateTime dateValue;
            if (!DateTime.TryParseExact(str, dateFormats,
                CultureInfo.InvariantCulture,//new CultureInfo("en-US"),
                DateTimeStyles.None,
                out dateValue))
            {
                if (!DateTime.TryParse(str, out dateValue))
                    dateValue = DateTime.Now;

            }
            return dateValue;
        }

        public void FillInformation()
        {
            DbParamCount = DT.Rows.Count;
        }

        public void TestFill(DataTable dt)
        {
            var row = dt.NewRow();
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@account_id"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 12; row["name"] = "@date_out"; row["name1"] = "datetime"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@territory_id"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@policy_id"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@address_id"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@address_use_kladr"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_kladr"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_okato"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_country"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_province"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_district"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_town"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_street"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_building"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_corpus"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_flat_office"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_zipcode"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@address_full"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@activity_field"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@activity_field_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@production_type"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@production_type_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@operating_mode"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@operating_mode_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@is_ownership"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@is_lease"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@lessor"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@lease_number"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 12; row["name"] = "@lease_date"; row["name1"] = "datetime"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_administrative"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_dwelling"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_market"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_production"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_warehouse"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@bldg_warehouse_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@bldg_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@bldg_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@bldg_year"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@bldg_repair_year"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@floors_count"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@floors"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@rooms_count"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@full_area"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@rooms_area"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@walls_rc"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@walls_brick"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@walls_metal"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@walls_wood"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@walls_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@walls_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@roof_soft"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@roof_slate"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@roof_metal"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@roof_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@roof_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@ceiling_rc"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@ceiling_metal"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@ceiling_wood"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@ceiling_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@ceiling_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@septum_rc"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@septum_brick"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@septum_gp"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@septum_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@septum_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@building_location"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_administrative"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_dwelling"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_market"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_production"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_warehouse"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@adj_bldg_warehouse_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@adj_bldg_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@adj_bldg_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@power_supply"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@power_supply_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@water_supply"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@water_supply_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@sewerage"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@sewerage_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@heating"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@heating_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@ventilation"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@ventilation_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@territory_type"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@territory_type_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@risk_objects"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_airport"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_port"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_railway"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_oil_factory"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_energy_complex"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_gas_station"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_under_construction"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@r_o_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@r_o_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@extinguishing"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@extinguishing_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@fire_alarm"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fire_alarm_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@fire_alarm_signal"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@fire_alarm_signal_type"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@monitoring_organization"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@m_o_contract_number"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 12; row["name"] = "@m_o_final_date"; row["name1"] = "datetime"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fire_out_signal_type"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@service_organization"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@s_o_contract_number"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 12; row["name"] = "@s_o_final_date"; row["name1"] = "datetime"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@extinguishing_system"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_auto_manual"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_sprinkler_drencher"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_water"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_foam"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_powder"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@e_s_gas"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@combustible_explosive"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@security_pass_type"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@security"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@security_police"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@security_pse"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@security_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@security_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@security_mode"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@security_mode_other"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@arms_fire"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@arms_gas"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@arms_no"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@security_alarm"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@sensors_volume"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@sensors_impact"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@sensors_contact"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@sensors_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@out_signals"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@out_signals_in"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@out_signals_out"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@out_signals_other"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@out_signals_other_note"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@video_control"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@block_elements"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@block_elements_fence"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@block_elements_door"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@block_elements_lattice"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@fh_address_id"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_kladr"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_okato"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_country"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_province"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_district"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_town"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_street"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_building"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_corpus"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_flat_office"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_zipcode"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_address_full"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_phone"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_distance"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@fh_time"; row["name1"] = "varchar"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@fire_regulations"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@fire_regulations_file"; row["name1"] = "numeric"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@is_debt"; row["name1"] = "int"; row["status2"] = 1; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 7; row["name"] = "@result_status"; row["name1"] = "int"; row["status2"] = 2; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 10; row["name"] = "@result_value"; row["name1"] = "numeric"; row["status2"] = 2; dt.Rows.Add(row);
            row = dt.NewRow(); row["usertype"] = 2; row["name"] = "@result_message"; row["name1"] = "varchar"; row["status2"] = 2; dt.Rows.Add(row);

        }

    }
}
