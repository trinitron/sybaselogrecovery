﻿using System;
using System.Data;
using System.Windows.Forms;

namespace SybaseLogRecovery
{
    public partial class ParamsViewer : Form
    {
        private readonly DataTable _dataTable;
        public ParamsViewer(DataTable dataTable)
        {
            _dataTable = dataTable;
            InitializeComponent();
        }

        private void ParamsViewer_Load(object sender, EventArgs e)
        {
            this.dataTable1BindingSource.DataSource = _dataTable;
            dataTable1DataGridView.Update();
            //dataTable1DataGridView.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCellsExceptHeader);

        }
    }
}
